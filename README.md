# Database Organization and Design Course 2023 #

My name is **Oleksii Hornostal**. I'm assistant professor at the Department of Computer Engineering and Programming in National Technical University "Kharkiv Polytechnic Institute". I'm your Lecturer in Database Organization and Design Course. I also work as .Net Software Engineer in Game Dev IT-company.
My contact email: oleksii.hornostal@khpi.edu.ua

### What do you need to do to start? ###
* Create a bitbucket account and create a repository or create GoogleDrive account with shared folder for reports 
* [Download and install MySQL Community Server.](https://dev.mysql.com/downloads/mysql/)
* [Download and install MySQL Workbench](https://dev.mysql.com/downloads/workbench/)
* [Download book - Full MySQL Tutorial](https://drive.google.com/file/d/1XG_f1P-ILR6wv7ENFMf4uCpoz0Vmx_wC/view?usp=sharing) - and use it when I do reference like this: **Check Book paragraph 1 pages 3-6**
* [Download report sample](https://drive.google.com/file/d/1TPT7DxIWsa-D7ZlFyhLdFrBOsZtv7Tp1/view?usp=sharing)

### We will have 16 study weeks and 16 themes with 16 tasks. I will add links over time ###

## Module 1 ##

1. [Introduction to Databases](https://bitbucket.org/OleksiiHornostal/database-organization-and-design/src/master/lab01/)
2. [Introduction to MySQL. Working with MySQL Workbench](https://bitbucket.org/OleksiiHornostal/database-organization-and-design/src/master/lab02/)
3. [Basic MySQL Syntax: SELECT, WHERE, JOIN, ORDER, GROUP BY scrips, SUM and COUNT functions](https://bitbucket.org/OleksiiHornostal/database-organization-and-design/src/master/lab03/)
4. [Database Backup (Export and Import operations)](https://bitbucket.org/OleksiiHornostal/database-organization-and-design/src/master/lab04/)
5. [Stored Procedure (without parameters and with parameters)](https://bitbucket.org/OleksiiHornostal/database-organization-and-design/src/master/lab05/)
6. [Database Management using commands: CREATE, INSERT, UPDATE, DELETE, DROP](https://bitbucket.org/OleksiiHornostal/database-organization-and-design/src/master/lab06/)
7. [Regular Expressions in MySQL](https://bitbucket.org/OleksiiHornostal/database-organization-and-design/src/master/lab07/)
8. [Triggers in MySQL](https://bitbucket.org/OleksiiHornostal/database-organization-and-design/src/master/lab08/)

## Module 2 ##

9. [Microsoft SQL Server Database Engine. Working with SQL Server Management Studio (SSMS)](https://bitbucket.org/OleksiiHornostal/database-organization-and-design/src/master/lab09/)
10. [PostgreSQL Database Engine. Working with pgAdmin](https://bitbucket.org/OleksiiHornostal/database-organization-and-design/src/master/lab10/)

Every week I will provide you theory and tasks via links above. You need to read theory carefully, do your homework, upload doc and pdf report to the bitbucket repository or to the GoogleDrive and send me via Telegram.

### How to finish this course successfully and get a good mark? ###

1. To get 5A you need to finish all 10 labs (Module 1 + Module 2) before 14 Decembver.
2. To get 4B you need to finish all 8 labs (Module 1) before 14 December.
3. To get 3D you need to finish at least 4 labs (Module 1) before 14 December.
4. For all other marks or if you want to improve your mark you need to finish 8 (Module 1) labs and prepare for exam (all exam questions will be taken from these pages).

###### Good luck! ######