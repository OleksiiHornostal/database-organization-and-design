# Laboratory work № 1. Introduction to Databases #

## Theory Block ##

Databases are a way to organize the storage of a large amount of information. There are two main database types: **Relational** & **Non-Relational**. What’s the difference? 

A **relational database** is structured, meaning the data is organized in tables. Many times, the data within these tables have relationships with one another, or dependencies. A **non relational** database is document-oriented, meaning, all information gets stored in more of a laundry list order. Within a single construct, or document, you will have all of your data listed out.

Some popular SQL (relational) database systems include:

* MySQL
* Oracle
* Microsoft SQL Server
* PostgreSQL
* MariaDB

Some popular NoSQL (non-relational) databases include:

* MongoDB
* Cassandra
* Redis
* Apache HBase
* Amazon DynamoDB

The Basic terms in databases:

* Each database consists of Tables.
* Each table consists of many rows and columns.
* Each new row contains data about one single entity (such as one product or one order line). This is called a record. For example, the first row in Table 1 is a record; it describes the A416 product, which is a box of nails that costs fourteen cents. The terms row and record are interchangeable.
* Each column (also called an attribute) contains one piece of data that relates to the record, called a tuple. Examples of attributes are the quantity of an item sold or the price of a product. An attribute, when referring to a database table, is called a field. For example, the data in the Description column in Table 1 are fields. The terms attribute and field are interchangeable.

**!!! Please note:** that for this course, we will be using one of the most popular relational (SQL) databases **MySQL**.

## Task ##

You need to invent and present in docx/odt/pdf report some kind of application area in which you may need to store a large amount of structured data and create 3 tables with at least 4 columns in each and specify the data type used. Each table must be filled with 5 lines with fictitious data.

An example of an applied area: *university* (tables: students, groups, subjects), *online store* (product, client, order), *catalog of computer programs* (computer programs, software licenses, software licensing), etc.

## What should be in (docx/odt/pdf) report? ##

1. The name of invented Area (ex. University).
2. 3 tables with columns.
3. 5 rows of sample data in each table. 

###### Good luck! ######