# Laboratory work № 2. Introduction to MySQL. Working with MySQL Workbench #

## Theory Block ##

MySQL is a fast, easy-to-use RDBMS (Relational DataBase Management System) being used for many small and big businesses. MySQL is developed, marketed and supported by MySQL AB, which is a Swedish company.

A Relational DataBase Management System (RDBMS) is a software that:

* Enables you to implement a database with tables, columns and indexes.
* Guarantees the Referential Integrity between rows of various tables.
* Updates the indexes automatically.
* Interprets an SQL query and combines information from various tables.

During this lab we will use MySQL Workbench Software. MySQL Workbench is a unified visual tool for database architects, developers, and DBAs. MySQL Workbench provides data modeling, SQL development, and comprehensive administration tools for server configuration, user administration, backup, and much more. MySQL Workbench is available on Windows, Linux and Mac OS X.

MySQL Workbench allows you to manipulate databases 2 ways: via UI and via SQL commands. During this lab we will use MySQL Workbench UI to create new DataBase and new tables with columns.

To check available datatypes please **Check Book Part V pages 503-510**

[Please check this video Tutorilal before starting tasks](https://www.youtube.com/watch?v=X_umYKqKaF0)

[Please check this tutorial to understand how to create new database in MySQL Workbench via GUI](https://www.quackit.com/mysql/workbench/create_a_database.cfm)

[Please check this tutorial to understand how to create new tables in MySQL Workbench via GUI](https://www.quackit.com/mysql/workbench/create_a_table.cfm)

## Task ##

1. Create new Database for you area from Lab#1.
2. Create 3 tables for you Database from Lab#1.
3. Right click on each table name, select command "SELECT" and then in new window add new rows to each table (at least 5 rows for each table). Be careful and specify the correct datatypes.
4. Use AutoIncrement for row ID.

## What should be in (docx/odt/pdf) report? ##

1. Screenshots of Database structure (left-side menu)
2. Screenshots of each table structure (columns and their datatypes)
3. Screenshots of all data in each table (use SELECT comand)

###### Good luck! ######