# Laboratory work № 3. Basic MySQL Syntax: SELECT, WHERE, JOIN, ORDER, GROUP BY scrips, SUM and COUNT functions #

## Theory Block ##

Today we will learn and use main MySQL commands:

1. The SQL **SELECT** command is used to fetch data from the MySQL database.
2. We have seen the SQL SELECT command to fetch data from a MySQL table. We can use a conditional clause called the **WHERE Clause** to filter out the results. Using this WHERE clause, we can specify a selection criteria to select the required records from a table.
3. You can use multiple tables in your single SQL query. The act of joining **(JOIN operation)** in MySQL refers to smashing two or more tables into a single table.
4. We have seen the SQL SELECT command to fetch data from a MySQL table. When you select rows, the MySQL server is free to return them in any order, unless you instruct it otherwise by saying how to sort the result. But, you sort a result set by adding an **ORDER BY** clause that names the column or columns which you want to sort.
5. The MySQL **GROUP BY** statement is used along with the SQL aggregate functions like SUM to provide means of grouping the result dataset by certain database table column(s).
6. The MySQL **SUM** aggregate function allows selecting the total for a numeric column.
7. The MySQL **COUNT** aggregate function is used to count the number of rows in a database table

Please **Check Book paragraph 1 pages 24-28** to check how to use SELECT, WHERE, JOIN, ORDER BY, GROUP BY and SUM/COUNT functions.

## Task ##

You need to create scripts with these commands and functions:

1. SELECT command
2. WHERE clause
3. ORDER BY command
4. GROUP BY command
5. SUM and COUNT functions

## What should be in (docx/odt/pdf) report? ##

1. Text (or Screenshot) of all created scripts for listed commands and functions 
2. Screenshots of execution results of all created scripts

###### Good luck! ######