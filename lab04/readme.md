# Laboratory work № 4. Database Backup (Export and Import operations) #

## Theory Block ##

Database backup is the process of backing up the operational state, architecture and stored data of database software. It enables the creation of a duplicate instance or copy of a database in case the primary database crashes, is corrupted or is lost.

Database backup is a way to protect and restore a database. It is performed through database replication and can be done for a database or a database server. Typically, database backup is performed by the RDBMS or similar database management software. Database administrators can use the database backup copy to restore the database to its operational state along with its data and logs. The database backup can be stored locally or on a backup server.

In MySQL Workbench we can use Table Data Export and Import Wizard to create backup and and import it to the database.

[Please check this url to Export and Import sql Database with all tables and data via MySQL Workbench GUI](https://dev.mysql.com/doc/workbench/en/wb-admin-export-import-management.html)

## Task ##

1. Create 2 types of MySQL Backups (Dump Project Folder and Self-Contained File).
2. Create temporary Schema (database).
3. Import backup to the new temporary database and check that all data was successfully resored.

## What should be in (docx/odt/pdf) report? ##

1. Screenshot of Table Data Export and Import Wizard
2. Screenshot of Dump Project Folder and its files
3. Screenshot of Self-Contained File
4. Screenshot of the new temporary database andrestored tables and data.

###### Good luck! ######