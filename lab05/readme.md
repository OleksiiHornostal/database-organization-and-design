# Laboratory work № 5. Stored Procedure (without parameters and with parameters) #

## Theory Block ##

A procedure (often called a stored procedure) is a subroutine like a subprogram in a regular computing language, stored in database. A procedure has a name, a parameter list, and SQL statement(s). All most all relational database system supports stored procedure, MySQL 5 introduce stored procedure.

To create Stored Procedure we will use command **CREATE PROCEDURE**. 

Please note that Stored Procedure can be without parameters and without parameters. There are 3 types of parametes:

* **IN** parameters. IN is the default mode. When you define an IN parameter in a stored procedure, the calling program has to pass an argument to the stored procedure. In addition, the value of an IN parameter is protected. It means that even the value of the IN parameter is changed inside the stored procedure, its original value is retained after the stored procedure ends. In other words, the stored procedure only works on the copy of the IN parameter.
* **OUT** parameters. The value of an OUT parameter can be changed inside the stored procedure and its new value is passed back to the calling program. Notice that the stored procedure cannot access the initial value of the OUT parameter when it starts.
* **INOUT** parameters. An INOUT  parameter is a combination of IN  and OUT  parameters. It means that the calling program may pass the argument, and the stored procedure can modify the INOUT parameter, and pass the new value back to the calling program.

[Please check this tutorial to understand how to create Stored Procedure without parameters](https://www.mysqltutorial.org/getting-started-with-mysql-stored-procedures.aspx)

[Please check this tutorial to understand how to create Stored Procedure with parameters](https://www.mysqltutorial.org/stored-procedures-parameters.aspx)

## Task ##

1. Create stored procedure without parameters (ex. calculate some sum or average value after groupping rows by some specific column)
2. Create stored procedure with parameters - the same operation but only for one group that can be filtered by passes IN parameter.

## What should be in (docx/odt/pdf) report? ##

1. Text or Screeshot of stored procedure without parameters
2. Text or Screeshot of stored procedure with parameters
3. Screenshot of results of StoredProcedures execution

###### Good luck! ######