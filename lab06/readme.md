# Laboratory work № 6. Database Management using commands: CREATE, INSERT, UPDATE, DELETE, DROP #

## Theory Block ##

Today we will learn MySQL commands to operate databases and tables:

1. The **CREATE TABLE** statement does exactly as the name suggests — it creates a table. However, you need to provide information about the table that you want MySQL to create. For example, the table name, the column names and their data types, default values, constraints, etc.
2. The **INSERT** statement allows you to add data to your database tables. This inserts data into one row. The order of the values provided must correspond with the columns that the values are to be inserted into.
3. The SQL **UPDATE** statement allows us to update the data in our database. Also we use the WHERE clause to specify the exact record we need to update.
4. Use the SQL **DELETE** statement to delete data from your database. Like the SELECT and UPDATE statements, the DELETE statement accepts a WHERE clause so that you can specify the exact record/s to delete.
5. To remove existing tables, you use the MySQL **DROP** TABLE statement. The DROP TABLE statement removes a table and its data permanently from the database. In MySQL, you can also remove multiple tables using a single DROP TABLE statement, each table is separated by a comma (,).

[Please check this tutorial to understand how use all reviewd commands](https://www.a2hosting.com/kb/developer-corner/mysql/managing-mysql-databases-and-users-from-the-command-line#Create-MySQLDatabasesand-Users)

## Task ##

You will need to do these task step-by-step (do screenshots one by one immediately because you will delete and update some data. Do screenshots of ALL these steps: 

1. Create the new temporary database for University with sql command (CREATE databse "DB_NAME")
2. Create new table UniversityGroup with sql commands (CREATE TABLE ...) in the new temporary database
3. Insert at least 5 rows in your new table using Insert command (INSERT INTO)
4. Update 1 row with sql command (UPDATE ... )
5. Delete another 1 row (DELETE)
6. Create full database backup
7. DROP the whole database

## What should be in (docx/odt/pdf) report? ##

1. Text or Screeshot of all created and executed commands
2. Screenshot of execution results of all commands
3. Screenshot of the temporary database backup

###### Good luck! ######