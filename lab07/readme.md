# Laboratory work № 7. Regular Expressions in MySQL #

## Theory Block ##

A regular expression is a powerful way of specifying a pattern for a complex search. This section discusses the functions and operators available for regular expression matching and illustrates, with examples, some of the special characters and constructs that can be used for regular expression operations.

To work with Regular Expression in MySQL please use the REGEXP and NOT REGEXP operators in WHERE clause after columns name.

[Please check this tutorial to understand what is Regular Expression and how they can be created and used](https://medium.com/factory-mind/regex-tutorial-a-simple-cheatsheet-by-examples-649dc1c3f285)

[Please use this online utility to test all types of Regular Expressions](https://regex101.com/)

[Please check this tutorial to understand how to use Regular Expressions in MySQL](https://www.mysqltutorial.org/mysql-regular-expression-regexp.aspx/)

## Task ##

You need to create 3 different Regular Expression for filtering data in each of you table.

## What should be in (docx/odt/pdf) report? ##

1. Text or Screeshot of all regular expressions with explanation of their structure
2. Screenshot of execution results of filtering data with created regular expressions

###### Good luck! ######