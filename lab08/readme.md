# Laboratory work № 8. Triggers in MySQL #

## Theory Block ##

In MySQL, a trigger is a stored program invoked automatically in response to an event such as insert, update, or delete that occurs in the associated table. For example, you can define a trigger that is invoked automatically before a new row is inserted into a table.

MySQL supports triggers that are invoked in response to the INSERT, UPDATE or DELETE event.

MySQL supports only row-level triggers. It doesn’t support statement-level triggers. A row-level trigger is activated for each row that is inserted, updated, or deleted.  For example, if a table has 100 rows inserted, updated, or deleted, the trigger is automatically invoked 100 times for the 100 rows affected.

Triggers can be used for these purposes:

1. Auto-generating some values when row is inserted (ex. current date and time). 
2. Auto-updating some values when row is updated (ex. date and time of row modification).
3. Calculating some values after row is updated.
4. Aduting action - backuping data during updating or deleting rows

Please check this links with tutorials to understand how to create some types of triggers in MySQL:

1. [MySQL Before Insert Trigger](https://www.mysqltutorial.org/mysql-triggers/mysql-before-insert-trigger/)
2. [MySQL Before Update Trigger](https://www.mysqltutorial.org/mysql-triggers/mysql-before-update-trigger/)
3. [MySQL Before Delete Trigger](https://www.mysqltutorial.org/mysql-triggers/mysql-before-delete-trigger/)
4. [MySQL Audit Triggers](http://www.it-iss.com/mysql/mysql-triggers-for-auditing/)

## Task ##

You need to create "Audit" table for one of tables in you database from prebious labs (Lab#1, Lab#2, etc). This table should contain all rows from original table + new Auto-Increment ID + column "ActionDateTime" + column "Action" (UPDATE or DELETE).

Then you need to create a full-backup trigger before update and delete actions and insert data to some "Audit" table with current date and time.

As the result you will have trigger that store row and the time to the Audit table when any row in table is modified (updated or deleted).

## What should be in (docx/odt/pdf) report? ##

1. Screenshot of new Audit table structure.
1. Text or Screeshot of created full backup Trigger.
3. Screenshot of updating row action and new row in Audit table.
4. Screenshot of deleting row action and new row in Audit table.

###### Good luck! ######