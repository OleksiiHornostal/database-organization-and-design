# Laboratory work № 9. Microsoft SQL Server Database Engine. Working with SQL Server Management Studio (SSMS) #

## Theory Block ##

Microsoft SQL Server is a relational database management system developed by Microsoft. As a database server, it is a software product with the primary function of storing and retrieving data as requested by other software applications — which may run either on the same computer or on another computer across a network.

[You can check this Tutorial before starting tasks](https://www.sqlservertutorial.net/)

[You can check this Article to learn about Data types in Microsoft SQL (Transact-SQL)](https://docs.microsoft.com/en-us/sql/t-sql/data-types/data-types-transact-sql?view=sql-server-ver15)

[You need to download Microsoft SQL Server 2019 Developer Edition](https://www.microsoft.com/en-us/sql-server/sql-server-downloads)

[You need to download Microsoft SQL Server Managment Studio (SSMS)](https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-ver15)

## Task (please use Microsoft SQL Server 2019 Developer Edition and SQL Server Managment Studio SSMS) ##

1. Create new Database for you area from Lab#1.
2. Create 3 tables for you Database from Lab#1 (use CREATE comand).
3. Insert test data (use INSERT command) and check tables with SELECT and JOIN commands
4. Update 1 row with sql command (UPDATE ... )
5. Delete another 1 row (DELETE)
6. Create full database backup
7. DROP the whole database
8. Restore Backup in the new Database and check that tables structures and all data were restored successfully

## What should be in (docx/odt/pdf) report? ##

1. Screenshots of Database structure
2. Screenshots of each table structure (columns and their datatypes)
3. Screenshots of all data in each table (use SELECT comand)
4. Screenshots of all executed commands
5. Screenshots Backups process, files and restoring result 

###### Good luck! ######