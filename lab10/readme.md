# Laboratory work № 10. PostgreSQL Database Engine. Working with pgAdmin #

## Theory Block ##

PostgreSQL, also known as Postgres, is a free and open-source relational database management system emphasizing extensibility and SQL compliance. It was originally named POSTGRES, referring to its origins as a successor to the Ingres database developed at the University of California, Berkeley.

[You can check this Tutorial before starting tasks](https://www.postgresqltutorial.com/)

[You can check this Article to learn about Data types in PostgreSQL](https://www.postgresql.org/docs/9.5/datatype.html)

[You need to download PostgreSQL (the latest version)](https://www.postgresql.org/download/)

[You need to download pgAdmin (the latest version that supports your version of PostgreSQL)](https://www.pgadmin.org/download/)

## Task (please use PostgreSQL and pgAdmin) ##

1. Create new Database for you area from Lab#1.
2. Create 3 tables for you Database from Lab#1 (use CREATE comand).
3. Insert test data (use INSERT command) and check tables with SELECT and JOIN commands
4. Update 1 row with sql command (UPDATE ... )
5. Delete another 1 row (DELETE)
6. Create full database backup
7. DROP the whole database
8. Restore Backup in the new Database and check that tables structures and all data were restored successfully

## What should be in (docx/odt/pdf) report? ##

1. Screenshots of Database structure
2. Screenshots of each table structure (columns and their datatypes)
3. Screenshots of all data in each table (use SELECT comand)
4. Screenshots of all executed commands
5. Screenshots Backups process, files and restoring result 

###### Good luck! ######